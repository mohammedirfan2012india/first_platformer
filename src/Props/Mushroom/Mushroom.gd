extends KinematicBody2D

export var spawn_id = -1

enum {NORMAL,FIERY,NONE=-1}
export(int,"normal","fiery") var type = NORMAL 

var gravity = 0
var velocity = Vector2.ZERO
var Player = null

const off = Vector2(0,-100)
# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func lose():
	gravity = 500
	set_collision_mask_bit(0,false)
	Player = null
	velocity.x = -velocity.x #fly behind player
	type = NONE # set to none type
	return null

func _physics_process(delta):
	if Player != null:
		position.y = Player.position.y+off.y
		velocity.x = Player.velocity.x 	
	
	velocity.y += gravity*delta
	if position.y >= 800:
		queue_free()
		print("Mushroom Dead")
	
	move_and_slide(velocity)
