extends Node2D


const min_X = 0
const max_X = 4144


# Called when the node enters the scene tree for the first time.
func _ready():
	#set camera limits
	$Camera.limit_left = min_X
	$Camera.limit_right  = max_X


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
