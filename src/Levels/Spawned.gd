extends Node

var spawn_dict = {}


# Called when the node enters the scene tree for the first time.
func _ready():
	#create spawn dictionary
	for child in get_children():
		if child.get("spawn_id")!=null:
			spawn_dict[child.spawn_id] = child
		child.visible = false

func set_spawn(key):
	var spawn = spawn_dict.get(key,null)
	if spawn != null:spawn.visible=true
	print("Hi!",spawn)
	return spawn 

