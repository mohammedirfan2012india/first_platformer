extends Camera2D

var player_target
var player_died

# Called when the node enters the scene tree for the first time.
func _ready():
	player_target = get_parent().get_node("PlayerGreen")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if player_died:
		return
	
	position = player_target.position
