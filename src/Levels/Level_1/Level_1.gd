extends Levels


const min_X = 0
const max_X = 4144
const max_Y = -200 
# Called when the node enters the scene tree for the first time.
func _ready():
	#set camera limits
	$Camera.limit_left = min_X
	$Camera.limit_right = max_X
	$Camera.limit_top	= max_Y
	$Camera.limit_bottom= get_viewport().get_visible_rect().size[1]


