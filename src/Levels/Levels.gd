extends Node2D
class_name Levels
# Declare member variables here. Examples:



# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	toggle_bgm()
	
func toggle_bgm():
		if(Input.is_action_just_pressed("toggle_bgm")):
			#toggle audio playing
			$AudioStreamPlayer.playing=!$AudioStreamPlayer.playing


func _on_PlayerGreen_lives_lost():# on losing lives updating hud etc
	$StatusDisplay.spawn_hearts()


func _on_PlayerGreen_game_over():
	pass # Replace with function body.


func _on_RestartTimer_timeout():
	get_tree().reload_current_scene()

func _on_PlayerHitTimer_timeout():
	print("end")
	$PlayerGreen.set_collision_mask_bit(1,false)#player fall through world
	$RestartTimer.start()

func _on_PlayerGreen_chance_lost():
	$AudioStreamPlayer.playing=false
	$PlayerGreen.playing = false #freeze inputs
	$PlayerGreen.get_node("AnimatedSprite").play("hit")
	$PlayerGreen.velocity = Vector2(0,100 * get_physics_process_delta_time())
	$PlayerHitTimer.start()
	#print($PlayerHitTimer)
	
	#get_tree().reload_current_scene()
