extends Actors

signal lives_lost
signal chance_lost
signal game_over

export var stomp_angle = 30
export var acc = 50
export var max_speed = 600
export var jump_force = -1200

var playing = true
var min_X
var max_X
var PlayerData
var StatusDisplay
var Mushroom

func _ready():
	#get and set player limits from parent
	Camera.player_died = false
	PlayerData =  get_node("/root/PlayerData")
	StatusDisplay = get_parent().get_node("StatusDisplay")
	min_X = get_parent().min_X
	max_X = get_parent().max_X
func _physics_process(delta):
	if position.y >= 800 and playing:
		Camera.player_died = true
		lose_life(PlayerData.lives)
	player_move()
	

func lose_life(n=1,half=false):
	
	if Mushroom!= null and Mushroom.type == Mushroom.NORMAL and n!=PlayerData.max_lives:#tank a damage regardless of point
		Mushroom = Mushroom.lose()
		return
	if half : n+=0.5
	PlayerData.lives = PlayerData.lives-n if PlayerData.lives-n >=0 else 0
	emit_signal("lives_lost")
	if PlayerData.lives == 0:
		lose_chance()

func lose_chance(n=1):
	PlayerData.chances = PlayerData.chances-n if PlayerData.chances-n >=0 else 0
	PlayerData.lives = 3#fills back life
	emit_signal("chance_lost")
	if PlayerData.chances == 0:
		emit_signal("game_over")

func player_move():
	if playing:
		if is_on_floor() and Input.is_action_pressed("ui_up"):
			$AnimatedSprite.play("jump")
			velocity.y = jump_force
			
		if Input.is_action_pressed("ui_left"):
			$AnimatedSprite.play("walk")
			$AnimatedSprite.flip_h = true
			velocity.x = clamp(velocity.x-acc,-max_speed,0)
		elif Input.is_action_pressed("ui_right"):
			$AnimatedSprite.play("walk")
			$AnimatedSprite.flip_h = false
			velocity.x = clamp(velocity.x+acc,0,max_speed)
		else:
			$AnimatedSprite.play("stand")
			velocity.x = 0
		#limit player position
		position.x = clamp(position.x,min_X,max_X)
	velocity = move_and_slide(velocity,up_dir)
	


func _on_Area2D_body_entered(body):
	if(body.is_in_group("Enemy")):
		#get collision vector
		var coll_vec = position - body.position
		var angle = ((180/PI)*float(coll_vec.normalized().angle_to(Vector2.UP)))
		print('%d \u00b0' % angle)
		#on stomp loose life
		if abs(angle) < 30 :#cause 1 damage to enemy 
			body.lose_life()
		else:#cause damage to player 
			var damage = do_damage(body.damage)
			lose_life(damage[0],damage[1])
			
	elif body.is_in_group("Mushroom"):#collided with mushroom
		if body.type == body.NORMAL:
			body.Player = self #pass self to mushroom
			body.position = position + body.off
			Mushroom = body #own mushroom


func _on_Area2D_body_exited(body):
	if(body.is_in_group("Enemy")):
		body.get_node("AnimatedSprite").play("move")
