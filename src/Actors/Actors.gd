extends KinematicBody2D
class_name Actors

export var gravity = 1500

var velocity = Vector2.ZERO
var up_dir = Vector2.UP
var Camera
const off = 50
func _ready():
	Camera = get_tree().get_current_scene().get_node("Camera")

func _physics_process(delta):
	velocity.y += gravity * delta

func do_damage(n):
	var int_n = int(n)
	return [int_n,int_n-n != 0]
