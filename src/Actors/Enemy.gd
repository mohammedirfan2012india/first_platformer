extends Actors
class_name Enemy
signal enemy_died

export var spawn_id = -1

var lives = 1
var damage = 1 #damage done by enemy
const limit_off = 50
const max_distance_from_player = 800 #max dist. from player before enemy starts to move
var Player

func _ready():
	Player = get_tree().get_current_scene().get_node("PlayerGreen")

func is_within_move_limit():#move only within limit
	if abs(Player.position.x-position.x)<=max_distance_from_player:
		return true
	else: return false
	

func lose_life(n=1):
	lives = lives-n if lives >=0 else 0
	$AnimatedSprite.play('dead')
	if lives == 0:
		emit_signal("enemy_died")
		
func spawn_bonus():#create new items on death from children
	var Spawned = get_tree().get_current_scene().get_node("Spawned")
	var spawn = Spawned.set_spawn(spawn_id)
	if spawn == null:
		print("Spawn Failed")
func _on_Enemy_died():
	velocity.y = 50 * get_physics_process_delta_time()
	set_collision_mask_bit(1,false)#fall through world
	if spawn_id >= 0 :spawn_bonus()

func _physics_process(delta):
	if position.x < Camera.limit_left -off or position.x > Camera.limit_right + off or position.y > 950:
		queue_free()
		print("Died")
