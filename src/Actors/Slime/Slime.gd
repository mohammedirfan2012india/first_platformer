extends Enemy

enum enemy_color {GREEN,BLUE}

export(int,"green","blue") var type = enemy_color.GREEN

var speed = 100


# Called when the node enters the scene tree for the first time.
func _ready():
	
	$AnimatedSprite.play("move")
	velocity.x = -speed
	if type == enemy_color.GREEN:#green slime
		$AnimatedSprite.self_modulate = Color(1,1,0)
	else:#blue_slime
		$AnimatedSprite.self_modulate = Color(0,1.5,5)
		lives = 2
		damage = 1.5
				

func init(type):
	self.type = type
	if type >= enemy_color.BLUE:
		$AnimatedSprite.self_modulate = Color(0,1,4)
		lives = 2

func _physics_process(delta):
	if is_within_move_limit():
		move_and_slide(velocity)
