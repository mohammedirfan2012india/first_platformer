extends CanvasLayer

export (PackedScene) var Hearts
export (Color) var fontColor = Color(1,0,0,1)

var PlayerData
var camera_offset
var scr_offset
var hearts=[]
var Camera # get camera instance of parent
# Called when the node enters the scene tree for the first time.
func _ready():
	PlayerData = get_node("/root/PlayerData")
	$ChanceDisplay.add_color_override("font_color", fontColor)
	set_chance()
	var i = 0
	for x in range(PlayerData.max_lives):
		var heart = Hearts.instance()
		heart.position = Vector2(64*(i+1)-i*10,32)
		hearts.append(heart)
		add_child(heart)
		i+=1
	spawn_hearts()
	#print($Camera.position)

func set_chance():
	$ChanceDisplay.text = "Chances: "+str(PlayerData.chances)

func do_damage(n):
	var int_n = int(n)
	return [int_n,int_n-n != 0]

func spawn_hearts():#display hearts based on lives remaining
	#show available hearts
	var value = do_damage(PlayerData.lives)
	var lives = value[0]
	var half =value[1]
	#print(lives,half)
	for i in range(lives):
		var new_heart = hearts[i]
		new_heart.is_full()
		new_heart.visible = true
	#if half a half heart
	if half and lives <= PlayerData.max_lives:
		hearts[lives].is_full(false)#toggle half lifeheart
		hearts[lives].visible = true
		lives = lives+1
	#hide other hearts
	for i in range(lives,PlayerData.max_lives):
		var new_heart = hearts[i]
		new_heart.visible=false
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	#move HUD along with camera
	pass
